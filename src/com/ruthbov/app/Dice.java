package com.ruthbov.app;

import java.util.Random;

public enum Dice {
    ROLL_ONE("1"),
    ROLL_TWO("2"),
    ROLL_THREE("3"),
    ROLL_FOUR("4"),
    ROLL_FIVE("5"),
    ROLL_SIX("6");

    private final String value;
    private static final Random random = new Random();
    private static final Dice[] dieFaces = Dice.values();
    Dice(String value) {
        this.value = value;
    }

    public void printValue() {
        System.out.println(value);
    }

    public static String getRandomDieFace() {
        String dieValue = dieFaces[random.nextInt(dieFaces.length)].value;
        System.out.println(dieValue);
        return dieValue;
    }
}
